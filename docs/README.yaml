---

name: terraform-comprehend
logo: docs/logo.jpeg

pipeline_status: "true"
commercial_support: "false"

repo: skalesys/terraform-comprehend

description: |-
  Terraform workspace that provision resources allowing telemetry and sentiment analysis with AWS Kinesis, S3 Bucket and AWS Comprehend services.

requirements:
  - name: "Ubuntu"
    url: "ubuntu"
    description: "Ubuntu is a complete Linux operating system, freely available with both community and professional support."

  - name: "Terraform"
    url: "terraform"
    description: "Write, Plan, and Create Infrastructure as Code"

# How to use this project
usage: |-
  ```
  make init install
  make terraform/plan STAGE=development
  make terraform/apply STAGE=development
  ```

  How to upload the `samples` to S3:
  ```
  aws s3 sync samples/ s3://sk-dev-analysis-stream/telemetry/
  ```

  How to send sample data to Kinesis Stream:
  ```
  make send/sample SAMPLE_NUMBER=1
  ```
  It will the sample `samples/1.txt`.

  Or to send all the samples:
  ```
  make send/samples
  ```

  How to list sentiment job detection via CLI:
  ```
  aws comprehend list-sentiment-detection-jobs
  ```

  How to described sentiment job detection via CLI:
  ```
  aws comprehend describe-sentiment-detection-job --job-id d130ec18e33dea8ed149a23d3d08746e

  Output:
  {
      "SentimentDetectionJobProperties": {
          "InputDataConfig": {
              "S3Uri": "s3://sk-dev-analysis-stream/",
              "InputFormat": "ONE_DOC_PER_LINE"
          },
          "DataAccessRoleArn": "arn:aws:iam::096373988534:role/service-role/AmazonComprehendServiceRole-ReadInputOutputS3Buckets",
          "LanguageCode": "en",
          "JobStatus": "IN_PROGRESS",
          "JobName": "sk-dev-analysis-sentiment",
          "SubmitTime": 1557208458.464,
          "OutputDataConfig": {
              "S3Uri": "s3://sk-dev-analysis-stream-sentiment-result/096373988534-SENTIMENT-d130ec18e33dea8ed149a23d3d08746e/output/output.tar.gz"
          },
          "JobId": "d130ec18e33dea8ed149a23d3d08746e"
      }
  }
  ```

screenshots:
  - name: "amazon-kinesis-stream"
    url: "docs/screenshots/0-amazon-kinesis-stream.png"
    description: "Amazon Kinesis Stream"

  - name: "amazon-kinesis-firehose"
    url: "docs/screenshots/1-amazon-kinesis-firehose.png"
    description: "Amazon Kinesis Firehose"

  - name: "amazon-comprehend-sentiment-analysis"
    url: "docs/screenshots/2-amazon-comprehend-sentiment-analysis.png"
    description: "Amazon Comprehend Sentiment document analysis"

  - name: "amazon-comprehend-sentiment-analysis-jobs"
    url: "docs/screenshots/3-amazon-comprehend-sentiment-analysis-jobs.png"
    description: "Amazon Comprehend Sentiment document analysis jobs"

  - name: "amazon-comprehend-sentiment-analysis-result"
    url: "docs/screenshots/4-amazon-comprehend-sentiment-analysis-result.png"
    description: "Amazon Comprehend sentiment document analysis result"

  - name: "amazon-comprehend-sentiment-analysis-bucket-result"
    url: "docs/screenshots/5-amazon-comprehend-sentiment-analysis-bucket-result.png"
    description: "Amazon Comprehend sentiment document analysis bucket result"

references:
  - name: "AWS Kinesis"
    description: "Amazon Kinesis makes it easy to collect, process, and analyze real-time, streaming data so you can get timely insights and react quickly to new information."
    url: "https://aws.amazon.com/kinesis/"

  - name: "AWS S3"
    description: "Amazon Simple Storage Service (Amazon S3) is an object storage service that offers industry-leading scalability, data availability, security, and performance. "
    url: "https://aws.amazon.com/s3/"

  - name: "AWS Comprehend"
    description: "Amazon Comprehend is a natural language processing (NLP) service that uses machine learning to find insights and relationships in text. No machine learning experience required."
    url: "https://aws.amazon.com/comprehend/"

resources:
  - name: "Photo"
    url: "https://unsplash.com/photos/ORYtC6R8omE"
    description: "by Dean Truderung on Unsplash"

  - name: "Gitignore.io"
    url: "https://gitignore.io"
    description: "Defining the `.gitignore`"

  - name: "LunaPic"
    url: "https://www341.lunapic.com/editor/"
    description: "Image editor (used to create the avatar)"

links:
  - name: "aws"
    url: "https://aws.amazon.com/"

contributors:
  - name: "Valter Silva"
    username: "valter-silva"

include:
  - "docs/targets.md"